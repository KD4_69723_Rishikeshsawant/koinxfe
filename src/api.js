// api.js

const BASE_URL = 'https://api.coingecko.com/api/v3';

export const fetchBitcoinData = async () => {
  try {
    const response = await fetch(`${BASE_URL}/simple/price?ids=bitcoin&vs_currencies=inr,usd&include_24hr_change=true`);
    const data = await response.json();
    return data.bitcoin;
  } catch (error) {
    console.error('Error fetching Bitcoin data:', error);
    return null;
  }
};

export const fetchTrendingCoins = async () => {
  try {
    const response = await fetch(`${BASE_URL}/search/trending`);
    const data = await response.json();
    return data.coins;
  } catch (error) {
    console.error('Error fetching trending coins:', error);
    return null;
  }
};
