import React from 'react';
import './App.css';
import Header from './components/Header';
import BitcoinChart from './components/BitcoinChart';
import TradingViewWidget from './components/TradingViewWidget';
import CryptoDetails from './components/CryptoDetails';
import SentimentSection from './components/SentimentSection';
import Advertise from './components/Advertise';

function App() {
  return (
    <div className="app">
      <Header />
      <div className="container">
        <div className="section">
          <h1>Cryptocurrencies >> Bitcoin</h1>
          <BitcoinChart />
        </div>
        <div className="section">
          <CryptoDetails />
        </div>
        <div className="section">
          <Advertise />
        </div>
      </div>
    </div>
  );
}

export default App;
