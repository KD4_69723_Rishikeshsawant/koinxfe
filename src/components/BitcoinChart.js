import React, { useState, useEffect } from 'react';
import TradingViewWidget from './TradingViewWidget';

function BitcoinChart() {
  const [bitcoinData, setBitcoinData] = useState({});

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(
          'https://api.coingecko.com/api/v3/simple/price?ids=bitcoin&vs_currencies=usd,inr&include_24hr_change=true'
        );
        const result = await response.json();
        setBitcoinData(result.bitcoin);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, []);

  return (
    <div className="bitcoin-chart-container">
      <h2>Bitcoin Chart</h2>
      {/* <div>
        <p>USD Price: {bitcoinData.usd}</p>
        <p>USD 24h Change: {bitcoinData.usd_24h_change}%</p>
        <p>INR Price: {bitcoinData.inr}</p>
        <p>INR 24h Change: {bitcoinData.inr_24h_change}%</p>
      </div> */}
      <div className="tradingview-widget-container">
        <TradingViewWidget />
      </div>
    </div>
  );
}

export default BitcoinChart;
//