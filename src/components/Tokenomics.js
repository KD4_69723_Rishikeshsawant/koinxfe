import React from 'react';
import './Tokenomics.css';

function Tokenomics() {
  return (
    <div className="tokenomics-container">
      <div className="tokenomics-section">
        <h2 className="tokenomics-title">Tokenomics</h2>
        <div className="tokenomics-circle">
          <svg viewBox="0 0 100 100">
            <circle cx="50" cy="50" r="40" fill="none" stroke="yellow" strokeWidth="20"></circle>
            <circle cx="50" cy="50" r="40" fill="none" stroke= "#007bff"strokeWidth="20"
              strokeDasharray="calc(80 * 251.32741228718345 / 100) 251.32741228718345"
              transform="rotate(-90 50 50)"></circle>
          </svg>
        </div>
        <div className="tokenomics-distribution">
          <div className="tokenomics-distribution-item">
            <div className="tokenomics-color tokenomics-blue"></div>
            <div>Crowdsale investors: 80%</div>
          </div>
          <div className="tokenomics-distribution-item">
            <div className="tokenomics-color tokenomics-yellow"></div>
            <div>Foundation: 20%</div>
          </div>
        </div>
        <p className="tokenomics-description">
          <strong>Initial Distribution</strong><br />
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam tempor magna sit amet augue luctus, at
          vestibulum felis ultricies. Curabitur dapibus eros vel enim faucibus, sit amet bibendum elit mollis.
          Integer gravida eget lectus eget ullamcorper.
        </p>
      </div>
    </div>
  );
}

export default Tokenomics;
