import React from 'react';
import './Insights.css';

// Sample cryptocurrency data (replace with actual data)
const cryptocurrencyData = [
  {
    name: 'Bitcoin',
    logo: 'https://via.placeholder.com/50x50',
    price: '$40,000',
  },
  {
    name: 'Ethereum',
    logo: 'https://via.placeholder.com/50x50',
    price: '$2,500',
  },
  {
    name: 'Litecoin',
    logo: 'https://via.placeholder.com/50x50',
    price: '$200',
  },
  {
    name: 'Ripple',
    logo: 'https://via.placeholder.com/50x50',
    price: '$1.50',
  },
  {
    name: 'Cardano',
    logo: 'https://via.placeholder.com/50x50',
    price: '$1.20',
  },
  {
    name: 'Chainlink',
    logo: 'https://via.placeholder.com/50x50',
    price: '$30',
  },
  {
    name: 'Stellar',
    logo: 'https://via.placeholder.com/50x50',
    price: '$0.70',
  },
  {
    name: 'Polkadot',
    logo: 'https://via.placeholder.com/50x50',
    price: '$40',
  },
];

function Insights() {
  return (
    <div className="insights-container">
      <h2 className="insights-title">New Insights</h2>
      <div className="cryptocurrency-list">
        {cryptocurrencyData.map((crypto, index) => (
          <div className="crypto-item" key={index}>
            <div className="crypto-logo">
              <img src={crypto.logo} alt={crypto.name} />
            </div>
            <div className="crypto-details">
              <div className="crypto-name">{crypto.name}</div>
              <div className="crypto-price">{crypto.price}</div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}

export default Insights;
