import React from 'react';

function SentimentSection() {
  return (
    <div>
      <h2>Sentiment Section</h2>
      <p><strong>Key Events</strong></p>
      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
      
        <div style={{ backgroundColor: '#e6f7ff', padding: '10px', width: '45%' }}>
          <p>Vivamus bibendum, tortor nec rutrum ultricies, risus ipsum ullamcorper arcu, in vehicula
            ligula lacus quis sem. Pellentesque habitant morbi tristique senectus et netus et malesuada
            fames ac turpis egestas. Vestibulum at elit non nulla ultrices fermentum. Duis at lacus
            vel leo feugiat dapibus. Mauris posuere nunc sit amet libero ultricies, sed auctor sem
            feugiat. Sed ac libero quis nibh dapibus vehicula. In hac habitasse platea dictumst.</p>
          </div>

        <div style={{ backgroundColor: '#e6ffe6', padding: '10px', width: '45%' }}>
          <p>
            Vivamus bibendum, tortor nec rutrum ultricies, risus ipsum ullamcorper arcu, in vehicula
            ligula lacus quis sem. Pellentesque habitant morbi tristique senectus et netus et malesuada
            fames ac turpis egestas. Vestibulum at elit non nulla ultrices fermentum. Duis at lacus
            vel leo feugiat dapibus. Mauris posuere nunc sit amet libero ultricies, sed auctor sem
            feugiat. Sed ac libero quis nibh dapibus vehicula. In hac habitasse platea dictumst.
          </p>
        </div>
        
      </div>
  <div><h2>Analyst Estimate</h2></div>


      <div style={{ display: 'flex', alignItems: 'center', width: '45%' }}>
          <div style={{ backgroundColor: '#e6ffe6', padding: '10px', borderRadius: '50%', textAlign: 'center' }}>
            <p style={{ margin: 70, fontWeight: 'bold', color:'green' }}><h1>76%</h1></p>
          </div>
          <div style={{ backgroundColor: '#00cc44', height: '20px', width: '80%', marginLeft: '10px' }}>
            <div style={{ width: '76%', height: '100%', backgroundColor: '#00cc44' }}></div>
          </div>
          <div style={{ marginLeft: '1px' }}>Buy</div>
          <div style={{ marginLeft: 'auto' }}>76%</div>
        </div>

        <div>
        <div style={{ marginLeft: '200px',marginTop:'10px'}}>Hold</div>
       
      <div style={{ marginTop: '20px', display: 'flex', marginLeft:'210px' }}>
        <div style={{ backgroundColor: '#cccccc', height: '20px', width: '8%' }}>
          <div style={{ width: '8%', height: '100%', backgroundColor: '#cccccc' }}></div>
        </div>
        <div style={{ marginLeft: '5px' }}>8%</div>
        </div>
<br></br>
        <div>
        <div style={{ backgroundColor: '#00cc44', height: '20px', width: '16%', marginLeft: '210px' }}>
            <div style={{ width: '16%', height: '100%', backgroundColor: '#00cc44' }}></div>
          </div>
          <div style={{ marginLeft: '170px',marginTop:'-35px' }}>Buy</div>
          <div style={{ marginLeft: '385px' }}>16%</div>
        </div>
      
      </div>
    </div>
  );
}

export default SentimentSection;
