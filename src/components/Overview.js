import React from 'react';
import './Overview.css';

function Overview({ bitcoinData }) {
  // Dummy values
  const todayLow = 46930.22;
  const todayHigh = 49343.83;
  const low52Weeks = 16930.22;
  const high52Weeks = 49743.83;
  const bitcoinPrice = 16815.46;
  const marketCap = 323507290047;
  const low24h = 16382.07;
  const high24h = 16874.12;
  const marketCapDominance = 38.343;
  const low7d = 16382.07;
  const high7d = 16874.12;
  const volumeMarketCap = 0.0718;
  const tradingVolume = 2324893;
  const allTimeHigh = 6253627;
  const allTimeHighChange = -75;
  const allTimeHighDate = "Nov 10, 2021";
  const marketCapRank = 1;
  const allTimeLow = 64.34;
  const allTimeLowChange = 2456;
  const allTimeLowDate = "Jul 07, 2013";

  // Calculate percentage for bars
  const todayLowPercentage = ((todayLow - low24h) / (high24h - low24h)) * 100;
  const todayHighPercentage = ((todayHigh - low24h) / (high24h - low24h)) * 100;
  const low52WeeksPercentage = ((low52Weeks - low24h) / (high24h - low24h)) * 100;
  const high52WeeksPercentage = ((high52Weeks - low24h) / (high24h - low24h)) * 100;

  // Style for bar chart
  const barStyle = {
    height: '20px',
    background: 'linear-gradient(to right, green, yellow, orange)',
    marginBottom: '10px',
    borderRadius: '5px',
  };

  return (
    <div className="tab-content">
      <div className="performance">
        <h2>Performance</h2>
        <div>
          <p>
            <strong>Todays low:</strong> ${todayLow}
            <br />
            <strong>Todays high:</strong> ${todayHigh}
          </p>
          <div className="bar-chart" style={{ width: '100%' }}>
            <div className="bar" style={{ width: `${todayHighPercentage}%`, ...barStyle }}></div>
          </div>
          <p>
            <strong>52W Low:</strong> ${low52Weeks}
            <br />
            <strong>52W high:</strong> ${high52Weeks}
          </p>
          <div className="bar-chart" style={{ width: '100%' }}>
            <div className="bar" style={{ width: `${high52WeeksPercentage}%`, ...barStyle }}></div>
          </div>
        </div>
      </div>
      {/* Placeholder for Fundamentals component */}
      
    </div>
  );
}

export default Overview;
