import React from 'react';
import './Team.css';

function Team() {
  return (
    <div className="team-container">
      <div className="team-member">
        <img src="https://via.placeholder.com/150" alt="Team Member 1" />
        <div className="team-member-details">
          <div className="team-member-name">John Doe</div>
          <div className="team-member-role">Developer</div>
          <div className="team-member-description">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis libero in urna aliquam.
          </div>
        </div>
      </div>
      <div className="team-member">
        <img src="https://via.placeholder.com/150" alt="Team Member 2" />
        <div className="team-member-details">
          <div className="team-member-name">Jane Smith</div>
          <div className="team-member-role">Designer</div>
          <div className="team-member-description">
            Sed euismod ligula vitae neque ullamcorper efficitur. Proin nec neque at orci convallis rutrum.
          </div>
        </div>
      </div>
      <div className="team-member">
        <img src="https://via.placeholder.com/150" alt="Team Member 3" />
        <div className="team-member-details">
          <div className="team-member-name">Mike Johnson</div>
          <div className="team-member-role">Marketing</div>
          <div className="team-member-description">
            Vivamus fringilla magna a libero consequat, non fringilla velit varius. Cras sed ante magna.
          </div>
        </div>
      </div>
    </div>
  );
}

export default Team;
