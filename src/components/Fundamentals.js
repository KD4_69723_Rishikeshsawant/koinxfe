import React from 'react';

function Fundamentals() {
  // Dummy values
  const bitcoinPrice = 16815.46;
  const marketCap = 323507290047;
  const low24h = 16382.07;
  const high24h = 16874.12;
  const marketCapDominance = 38.343;
  const low7d = 16382.07;
  const high7d = 16874.12;
  const volumeMarketCap = 0.0718;
  const tradingVolume = 2324893;
  const allTimeHigh = 6253627;
  const allTimeHighChange = -75;
  const allTimeHighDate = "Nov 10, 2021";
  const marketCapRank = 1;
  const allTimeLow = 64.34;
  const allTimeLowChange = 2456;
  const allTimeLowDate = "Jul 07, 2013";

  return (
    <div className="fundamentals">
      <h2>Fundamentals</h2>
      <div>
        <p>
          <strong>Bitcoin price:</strong> ${bitcoinPrice}
        </p>
        <p>
          <strong>Market cap:</strong> ${marketCap.toLocaleString()}
        </p>
        <p>
          <strong>24h low/24h high:</strong> ${low24h} - ${high24h}
        </p>
        <p>
          <strong>Market cap dominance:</strong> {marketCapDominance}%
        </p>
        <p>
          <strong>7d low/7d high:</strong> ${low7d} - ${high7d}
        </p>
        <p>
          <strong>Volume/market cap:</strong> {volumeMarketCap}
        </p>
        <p>
          <strong>Trading volume:</strong> ${tradingVolume.toLocaleString()}
        </p>
        <p>
          <strong>All time high:</strong> ${allTimeHigh} ({allTimeHighChange}%) ({allTimeHighDate})
        </p>
        <p>
          <strong>Market cap rank:</strong> #{marketCapRank}
        </p>
        <p>
          <strong>All time low:</strong> ${allTimeLow} ({allTimeLowChange}%) ({allTimeLowDate})
        </p>
      </div>
    </div>
  );
}

export default Fundamentals;
