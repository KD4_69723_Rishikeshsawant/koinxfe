import React from 'react';
import './Advertise.css'; // Import the CSS file

const Advertise = () => {
  return (
    <div className="advertise">
      <div className="advertise-content">
        <h2 className="advertise-title">Get Started with Coins for FREE</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin varius massa eget ligula gravida.</p>
        <p>Sed ac libero quis nibh dapibus vehicula. In hac habitasse platea dictumst.</p>
        <p>Vivamus bibendum, tortor nec rutrum ultricies, risus ipsum ullamcorper arcu, in vehicula ligula lacus quis sem.</p>
        <img src="path_to_your_image.jpg" alt="Advertise" className="advertise-image" />
        <button className="get-started-button">Get Started for FREE</button>
      </div>
    </div>
  );
};

export default Advertise;
