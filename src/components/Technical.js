import React from 'react';
import './Technical.css';

function Technical() {
  return (
    <div className="technical-container">
      <div className="technical-section">
        <h2 className="technical-title">About Bitcoin</h2>
        <p className="technical-description">
          <strong>What is Bitcoin?</strong><br />
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam tempor magna sit amet augue luctus, at
          vestibulum felis ultricies. Curabitur dapibus eros vel enim faucibus, sit amet bibendum elit mollis.
          Integer gravida eget lectus eget ullamcorper.
        </p>
        <p className="technical-description">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam tempor magna sit amet augue luctus, at
          vestibulum felis ultricies. Curabitur dapibus eros vel enim faucibus, sit amet bibendum elit mollis.
          Integer gravida eget lectus eget ullamcorper.
        </p>
        <p className="technical-description">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam tempor magna sit amet augue luctus, at
          vestibulum felis ultricies. Curabitur dapibus eros vel enim faucibus, sit amet bibendum elit mollis.
          Integer gravida eget lectus eget ullamcorper.
        </p>
      </div>
      <div className="holding-section">
        <h1 className="holding-title">Already Holding Bitcoin?</h1>
        <div className="holding-images">
          <img src="https://via.placeholder.com/150" alt="Bitcoin 1" />
          <img src="https://via.placeholder.com/150" alt="Bitcoin 2" />
        </div>
        <p className="holding-description">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam tempor magna sit amet augue luctus, at
          vestibulum felis ultricies. Curabitur dapibus eros vel enim faucibus, sit amet bibendum elit mollis.
          Integer gravida eget lectus eget ullamcorper.
        </p>
      </div>
    </div>
  );
}

export default Technical;
