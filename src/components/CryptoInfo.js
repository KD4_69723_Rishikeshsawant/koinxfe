import React, { useState, useEffect } from 'react';

function CryptoInfo() {
  const [bitcoinData, setBitcoinData] = useState(null);

  useEffect(() => {
    fetch('https://api.coingecko.com/api/v3/simple/price?ids=bitcoin&vs_currencies=inr,usd&include_24hr_change=true')
      .then(response => response.json())
      .then(data => {
        setBitcoinData(data.bitcoin);
      })
      .catch(error => {
        console.error('Error fetching Bitcoin data:', error);
      });
  }, []);

  return (
    <div className="crypto-info">
      {bitcoinData && (
        <div className="crypto-item">
          <div className="crypto-name">Bitcoin (BTC)</div>
          <div className="crypto-price">
            <div>USD: ${bitcoinData.usd}</div>
            <div>INR: ₹{bitcoinData.inr}</div>
          </div>
          <div className="crypto-change">
            24H Change: {bitcoinData.usd_24h_change > 0 ? '+' : ''}{bitcoinData.usd_24h_change}%
          </div>
        </div>
      )}
    </div>
  );
}

export default CryptoInfo;
