import React, { useState, useEffect } from 'react';
import Overview from './Overview';
import SentimentSection from './SentimentSection'; // Import the SentimentSection component
import Fundamentals from './Fundamentals';
import Technical from './Technical';
import Tokenomics from './Tokenomics';
import './CryptoDetails.css';
import Team from './Team';
import Insights from './Insights';

function CryptoDetails() {
  const [activeTab, setActiveTab] = useState('overview');
  const [bitcoinData, setBitcoinData] = useState(null);

  useEffect(() => {
    // Fetch Bitcoin data
    fetch('https://api.coingecko.com/api/v3/coins/bitcoin')
      .then(response => response.json())
      .then(data => {
        setBitcoinData(data);
      })
      .catch(error => {
        console.error('Error fetching Bitcoin data:', error);
      });
  }, []);

  const handleTabClick = (tab) => {
    setActiveTab(tab);
  };

  return (
    <div className="crypto-details-container">
      <div className="crypto-details-navbar">
        <button
          className={activeTab === 'overview' ? 'active' : ''}
          onClick={() => handleTabClick('overview')}
        >
          Overview
        </button>
        <button
          className={activeTab === 'fundamentals' ? 'active' : ''}
          onClick={() => handleTabClick('fundamentals')}
        >
          Fundamentals
        </button>
        <button
          className={activeTab === 'news' ? 'active' : ''}
          onClick={() => handleTabClick('news')}
        >
          News Insights
        </button>
        <button
          className={activeTab === 'sentiments' ? 'active' : ''}
          onClick={() => handleTabClick('sentiments')}
        >
          Sentiments
        </button>
        <button
          className={activeTab === 'team' ? 'active' : ''}
          onClick={() => handleTabClick('team')}
        >
          Team
        </button>
        <button
          className={activeTab === 'technicals' ? 'active' : ''}
          onClick={() => handleTabClick('technicals')}
        >
          Technicals
        </button>
        <button
          className={activeTab === 'tokenomics' ? 'active' : ''}
          onClick={() => handleTabClick('tokenomics')}
        >
          Tokenomics
        </button>
      </div>
      <div className="crypto-details-content">
        {activeTab === 'overview' && bitcoinData && (
          <Overview bitcoinData={bitcoinData} />
        )}
        {activeTab === 'fundamentals' && (
          <Fundamentals/>
        )}
        {activeTab === 'news' && (
          <Insights/>
        )}
        {activeTab === 'sentiments' && (
          <SentimentSection /> // Render SentimentSection component only when 'sentiments' tab is active
        )}
        {activeTab === 'team' && (
          <Team/>
        )}
        {activeTab === 'technicals' && (
         <Technical />
        )}
        {activeTab === 'tokenomics' && (
          <Tokenomics />
        )}
      </div>
    </div>
  );
}

export default CryptoDetails;
