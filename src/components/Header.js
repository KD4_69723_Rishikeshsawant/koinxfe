
import React from 'react';

const Header = () => {
  return (
    <div className="header">
      <div className="logo">KoinX</div>
      <nav>
        <ul>
          <li className="right">Crypto Taxes</li>
          <li className="right">Free Tools</li>
          <li className="right">Resource Center</li>
        </ul>
      </nav>
      <button className="get-started-btn">Get Started</button>
    </div>
  );
};

export default Header;
