import React, { useState, useEffect, useRef, memo } from 'react';

function TradingViewWidget() {
  const container = useRef();
  const [bitcoinPrices, setBitcoinPrices] = useState(null);

  useEffect(() => {
    // Fetch Bitcoin prices
    fetch('https://api.coingecko.com/api/v3/simple/price?ids=bitcoin&vs_currencies=inr,usd&include_24hr_change=true')
      .then(response => response.json())
      .then(data => {
        setBitcoinPrices(data.bitcoin);
      })
      .catch(error => {
        console.error('Error fetching Bitcoin prices:', error);
      });

    // Check if TradingView script has already been loaded
    if (!window.TradingView) {
      const script = document.createElement("script");
      script.src = "https://s3.tradingview.com/external-embedding/embed-widget-advanced-chart.js";
      script.type = "text/javascript";
      script.async = true;
      script.innerHTML = `
        {
          "autosize": true,
          "symbol": "BITSTAMP:BTCUSD",
          "interval": "W",
          "timezone": "INR/USD",
          "theme": "light",
          "style": "2",
          "locale": "en",
          "enable_publishing": false,
          "backgroundColor": "rgba(255, 255, 255, 1)",
          "hide_legend": true,
          "withdateranges": true,
          "save_image": false,
          "calendar": false,
          "hide_volume": true,
          "support_host": "https://www.tradingview.com"
        }`;
      container.current.appendChild(script);

      // Set a global variable to prevent re-loading the script
      window.TradingView = true;
    }
  }, []);

  return (
    <div className="tradingview-widget-container" style={{ height: "100%", width: "100%" }}>
      <div className="bitcoin-prices">
        {bitcoinPrices && (
          <div>
            <div style={{ fontWeight: 'bold', fontSize: '24px' }}>${bitcoinPrices.usd.toFixed(2)}</div>
            <div>₹{bitcoinPrices.inr.toFixed(2)}</div>
            <div className={bitcoinPrices.usd_24h_change > 0 ? 'green-box' : 'red-box'}>
              {bitcoinPrices.usd_24h_change > 0 ? '+' : ''}{bitcoinPrices.usd_24h_change.toFixed(2)}%
            </div>
          </div>
        )}
      </div>
      <div className="tradingview-widget-container__widget" ref={container} style={{ height: "calc(100% - 32px)", width: "100%" }}></div>
      <div className="tradingview-widget-copyright">
        <a href="https://www.tradingview.com/" rel="noopener nofollow" target="_blank"></a>
      </div>
    </div>
  );
}

export default memo(TradingViewWidget);
