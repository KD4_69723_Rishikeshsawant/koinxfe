// src/api/data.js
export const cryptoInfoData = {
    overview: 'Placeholder Overview text',
    fundamentals: 'Placeholder Fundamentals text',
    newsInsights: 'Placeholder News Insights text',
    sentiments: 'Placeholder Sentiments text',
    team: 'Placeholder Team text',
    technicals: 'Placeholder Technicals text',
    tokenomics: 'Placeholder Tokenomics text',
  };
  
  export const sentimentData = {
    sentimentText: 'Placeholder Sentiment text',
  };
  